#![cfg_attr(not(feature = "std"), no_std)]

use ink_lang as ink;

#[ink::contract]
mod cennznet_contract {
    #[cfg(not(feature = "ink-as-dependency"))]
    use ink_storage::{collections::HashMap as StorageHashMap, Lazy};

    #[ink(storage)]
    pub struct CennznetContract {
        owner: AccountId,
        total_supply: Lazy<Balance>,
        balances: StorageHashMap<AccountId, Balance>,
    }

    #[ink(event)]
    pub struct Transfer {
        #[ink(topic)]
        from: Option<AccountId>,
        #[ink(topic)]
        to: AccountId,
        value: Balance,
    }

    #[derive(Debug, PartialEq, Eq, scale::Encode, scale::Decode)]
    #[cfg_attr(feature = "std", derive(scale_info::TypeInfo))]
    pub enum Error {
        InsufficientBalance,
    }

    pub type Result<T> = core::result::Result<T, Error>;

    impl CennznetContract {
        #[ink(constructor)]
        pub fn new(initial_supply: Balance) -> Self {
            let owner = Self::env().caller();
            let mut balances = StorageHashMap::new();
            balances.insert(owner, initial_supply);

            Self::env().emit_event(Transfer {
                from: None,
                to: owner,
                value: initial_supply,
            });

            Self {
                owner,
                balances,
                total_supply: Lazy::new(initial_supply),
            }
        }

        #[ink(message)]
        pub fn total_supply(&self) -> Balance {
            *self.total_supply
        }

        #[ink(message)]
        pub fn balance_of(&self, owner: AccountId) -> Balance {
            self.balances.get(&owner).copied().unwrap_or(0)
        }

        #[ink(message)]
        pub fn transfer(&mut self, to: AccountId, value: Balance) -> Result<()> {
            let from = self.env().caller();
            self.transfer_from_to(from, to, value)
        }

        fn transfer_from_to(
            &mut self,
            from: AccountId,
            to: AccountId,
            value: Balance,
        ) -> Result<()> {
            let from_balance = self.balance_of(from);
            if from_balance < value {
                return Err(Error::InsufficientBalance);
            }
            self.balances.insert(from, from_balance - value);
            let to_balance = self.balance_of(to);
            self.balances.insert(to, to_balance + value);

            self.env().emit_event(Transfer {
                from: Some(from),
                to,
                value,
            });
            Ok(())
        }
    }

    #[cfg(not(feature = "ink-experimental-engine"))]
    #[cfg(test)]
    mod tests {
        use super::*;

        use ink_env::topics::PrefixedValue;
        use ink_env::AccountId;
        use ink_lang as ink;

        type Event = <CennznetContract as ::ink_lang::BaseEvent>::Type;

        fn encoded_into_hash<T>(entity: &T) -> Hash
        where
            T: scale::Encode,
        {
            use ink_env::{
                hash::{Blake2x256, CryptoHash, HashOutput},
                Clear,
            };

            let mut result = Hash::clear();
            let len_result = result.as_ref().len();
            let encoded = entity.encode();
            let len_encoded = encoded.len();

            if len_encoded <= len_result {
                result.as_mut()[..len_encoded].copy_from_slice(&encoded);
                return result;
            }

            let mut hash_output = <<Blake2x256 as HashOutput>::Type as Default>::default();
            <Blake2x256 as CryptoHash>::hash(&encoded, &mut hash_output);
            let copy_len = core::cmp::min(hash_output.len(), len_result);
            result.as_mut()[..copy_len].copy_from_slice(&hash_output[0..copy_len]);
            result
        }

        fn assert_transfer_event(
            event: &ink_env::test::EmittedEvent,
            expected_from: Option<AccountId>,
            expected_to: AccountId,
            expected_value: Balance,
        ) {
            let decoded_event = <Event as scale::Decode>::decode(&mut &event.data[..])
                .expect("encountered invalid contract event data buffer");

            match decoded_event {
                Event::Transfer(Transfer { from, to, value }) => {
                    assert_eq!(from, expected_from, "encountered invalid Transfer.from");
                    assert_eq!(to, expected_to, "encountered invalid Transfer.to");
                    assert_eq!(value, expected_value, "encountered invalid Transfer.value");
                } // _ => panic!("encountered unexpected event kind: expected a Transfer event"),
            }

            let expected_topics = vec![
                encoded_into_hash(&PrefixedValue {
                    prefix: b"",
                    value: b"CennznetContract::Transfer",
                }),
                encoded_into_hash(&PrefixedValue {
                    prefix: b"CennznetContract::Transfer::from",
                    value: &expected_from,
                }),
                encoded_into_hash(&PrefixedValue {
                    prefix: b"CennznetContract::Transfer::to",
                    value: &expected_to,
                }),
                encoded_into_hash(&PrefixedValue {
                    prefix: b"CennznetContract::Transfer::value",
                    value: &expected_value,
                }),
            ];

            for (n, (actual_topic, expected_topic)) in
                event.topics.iter().zip(expected_topics).enumerate()
            {
                let topic = actual_topic
                    .decode::<Hash>()
                    .expect("encountered invalid topic encoding");
                assert_eq!(topic, expected_topic, "encountered invalid topic at {}", n);
            }
        }

        #[ink::test]
        fn new_works() {
            let _contract = CennznetContract::new(100);
            let emit_events = ink_env::test::recorded_events().collect::<Vec<_>>();
            assert_eq!(emit_events.len(), 1);
            assert_transfer_event(&emit_events[0], None, AccountId::from([0x01; 32]), 100);
        }

        #[ink::test]
        fn balance_of_works() {
            let contract = CennznetContract::new(100);
            let emit_events = ink_env::test::recorded_events().collect::<Vec<_>>();
            assert_eq!(emit_events.len(), 1);
            assert_transfer_event(&emit_events[0], None, AccountId::from([0x01; 32]), 100);

            let accounts = ink_env::test::default_accounts::<ink_env::DefaultEnvironment>()
                .expect("Failed to get default accounts.");

            assert_eq!(contract.balance_of(accounts.alice), 100);
            assert_eq!(contract.balance_of(accounts.bob), 0);
        }

        #[ink::test]
        fn transfer_works() {
            // Constructor works.
            let mut contract = CennznetContract::new(100);
            // Transfer event triggered during initial construction.
            let accounts = ink_env::test::default_accounts::<ink_env::DefaultEnvironment>()
                .expect("Cannot get accounts");

            assert_eq!(contract.balance_of(accounts.bob), 0);
            // Alice transfers 10 tokens to Bob.
            assert_eq!(contract.transfer(accounts.bob, 10), Ok(()));
            // Bob owns 10 tokens.
            assert_eq!(contract.balance_of(accounts.bob), 10);

            let emitted_events = ink_env::test::recorded_events().collect::<Vec<_>>();
            assert_eq!(emitted_events.len(), 2);
            // Check first transfer event related to ERC-20 instantiation.
            assert_transfer_event(&emitted_events[0], None, AccountId::from([0x01; 32]), 100);
            // Check the second transfer event relating to the actual transfer.
            assert_transfer_event(
                &emitted_events[1],
                Some(AccountId::from([0x01; 32])),
                AccountId::from([0x02; 32]),
                10,
            );
        }

        #[ink::test]
        fn invalid_transfer_should_fail() {
            let mut contract = CennznetContract::new(100);
            let accounts = ink_env::test::default_accounts::<ink_env::DefaultEnvironment>()
                .expect("Failed to get default accounts");
            assert_eq!(contract.balance_of(accounts.bob), 0);

            let callee =
                ink_env::account_id::<ink_env::DefaultEnvironment>().unwrap_or([0x0; 32].into());
            let data = ink_env::test::CallData::new(ink_env::call::Selector::new([0x00; 4]));
            ink_env::test::push_execution_context::<ink_env::DefaultEnvironment>(
                accounts.bob,
                callee,
                1000000,
                1000000,
                data,
            );

            // Bob fails to transfers 10 tokens to Eve.
            assert_eq!(
                contract.transfer(accounts.eve, 10),
                Err(Error::InsufficientBalance)
            );
            // Alice owns all the tokens.
            assert_eq!(contract.balance_of(accounts.alice), 100);
            assert_eq!(contract.balance_of(accounts.bob), 0);
            assert_eq!(contract.balance_of(accounts.eve), 0);

            // Transfer event triggered during initial construction.
            let emitted_events = ink_env::test::recorded_events().collect::<Vec<_>>();
            assert_eq!(emitted_events.len(), 1);
            assert_transfer_event(&emitted_events[0], None, AccountId::from([0x01; 32]), 100);
        }
    }
}
